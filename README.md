## PART 1 ##
1. Install Advanced Yii2 template;
1. Working only with "fronetend" directory;
1. Create UserList action in SiteController;
1. Show all users with all fields in database (all dates convert to type: 'May 27, 1995' using Yii formatter) if the user is logged in. If not show only the link to login page. Check user authorisation using global Yii Identity component;
1. Create your own pagination widget to show users (10 users by page) !
**### DO NOT USE Yii2 LinkPager Widget; ###**
## PART 2 ##
2. Create "GridView" action in SiteController;
2. Show all users using GridView Widget (without any actions such as View, Delete and Edit);
## PART 3 ##
3. Create "ActiveForm" action in SIteController;
3. Create User Registration form using yii2 ActiveForm widget(username, email, password);
3. Add custom validation using Yii ActiveForm JS:
    - username: max-length: 50, min-length: 5, symbols: [Aa-Zz_-],
    - email: should only end with "@gmail.com",
    - password: should have at least 1 capital letter
### **DO NOT USE MASKS!!!** ###
## PART 4 ##
4. Create new branch with your name in the repository;
4. Push there your updates;

**Sources**

1. [https://yiiframework.com.ua/ru/doc/guide/2/start-installation/](Link URL)

1. [http://www.yiiframework.com/doc-2.0/index.html](Link URL)